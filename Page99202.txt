OBJECT Page 99202 Presence Entry List
{
  OBJECT-PROPERTIES
  {
    Date=08.03.18;
    Time=10:02:05;
    Modified=Yes;
    Version List=RDN.VVK;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Presence Entry;
               ISL=Vi�veruskr�ning];
    SourceTable=Table99201;
    PageType=List;
  }
  CONTROLS
  {
    { 1100410000;0;Container;
                ContainerType=ContentArea }

    { 1100410001;1;Group  ;
                Name=Group;
                GroupType=Repeater }

    { 1100410002;2;Field  ;
                SourceExpr="Employee No." }

    { 1100410003;2;Field  ;
                SourceExpr="Start Date" }

    { 1100410004;2;Field  ;
                SourceExpr="End Date" }

    { 1100410005;2;Field  ;
                SourceExpr=Status }

    { 1100410006;2;Field  ;
                SourceExpr=Comment }

  }
  CODE
  {

    BEGIN
    END.
  }
}

